import pickle
import re

def date_sort(lines):
    r = re.search("\d\d:\d\d-\d\d:\d\d", lines)
    return r.group(0) if r else '&'

def clear(data):
    for i in data:
        data[i] = list(set(data[i]))
        data[i].sort(key=lambda l: date_sort(l))
    return data

def parse_data(records, number):
    temp_work = ''
    virus_words = ['Время', 'Аудитория', 'Предмет', 'Преподаватель']
    shedule = {'Понедельник':[], 'Вторник':[], 'Среда':[], 'Четверг':[], 'Пятница':[], 'Суббота':[]}
    for record in records:
        if record in virus_words:
            continue
        elif record in shedule.keys():
            if temp_work: shedule[day_week].append(temp_work)
            day_week = record
            temp_work = ''
        elif record[0].isdecimal():
            if temp_work: shedule[day_week].append(temp_work)
            temp_work = number + ', ' + record
        elif record == records[-1]:
            temp_work += ', ' + record
            if temp_work: shedule[day_week].append(temp_work)
        else:
            temp_work += ', ' + record
    return shedule


def poisk_data(poisk, shedule_file_name='all_shedule', numbers_group_file_name='numbers_groups'):
    result = {'Понедельник':[], 'Вторник':[], 'Среда':[], 'Четверг':[], 'Пятница':[], 'Суббота':[]}
    numbers = load_obj(numbers_group_file_name)
    shedule = load_obj(shedule_file_name)
    for group in numbers:
        for day in result.keys():
            for rows in shedule[group][day]:
                if poisk in rows:
                    result[day].append(rows)
    return clear(result)


def save_obj(obj, name ):
    with open('./' + name + '.pkl', 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)


def load_obj(name ):
    with open('./' + name + '.pkl', 'rb') as f:
        return pickle.load(f)


def output(keyword):
    days = ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота']
    message = ''
    poisk = keyword
    if poisk == 'dump':
        message += '[-] Сайт можно скопировать только в локальной версии'
        return message
    else:
        shedule = poisk_data(poisk)
        message += '\n'
        for i in days:
            message += '\n'
            if i == 'Понедельник':
                message += '*' * 7 + i + '*' * 5
            elif i == 'Среда':
                message += '*' * 7 + i + '*' * 8
            else:
                message += '*' * 7 + i + '*' * 7
            for j in shedule[i]:
                message += '\n[+] ' + re.sub("^\s+|\n|\r|\t|\s+$", '', j)
        message += '\n\n[=] Поиск успешно произведен!'
        return message
