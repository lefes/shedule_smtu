# -*- coding: utf-8 -*-
import telebot
import config_bot
import parse_shedule
import botan

bot = telebot.TeleBot(config_bot.token)

@bot.message_handler(content_types=["text"])
def repeat_all_messages(message):
    if message.text == 'dump':
        bot.send_message(message.chat.id, 'Скопировать сайт можно только в офлайн версии')
    else:
        response = parse_shedule.output(message.text)
        if len(response) >= 4096:
            for j in [response[i:i + 4096] for i in range(0, len(response), 4096)]:
                bot.send_message(message.chat.id, j)
        else:
            bot.send_message(message.chat.id, response)
    botan.track(config_bot.botan_key, message.chat.id, message, 'Использование')
	
if __name__ == '__main__':
	bot.polling(none_stop=True)
