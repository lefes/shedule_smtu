from bs4 import BeautifulSoup
from urllib import request
import pickle
import lxml
import re

def date_sort(lines):
    r = re.search("\d\d:\d\d-\d\d:\d\d", lines)
    return r.group(0) if r else '&'

def clear(data):
    for i in data:
        data[i] = list(set(data[i]))
        data[i].sort(key=lambda l: date_sort(l))
    return data

def parse_numbers_groups():
    numbers_groups = []
    page = request.urlopen(url="https://www.smtu.ru/ru/listschedule/")
    soup = BeautifulSoup(page, 'lxml')
    for href in soup.find_all('a', href=True):
        if href['href'][-5:-1].isdecimal():
            numbers_groups.append(href['href'][-5:-1])
    save_obj(numbers_groups, 'numbers_groups')
    return numbers_groups


def parse_group(number):
    data = []
    group = "https://www.smtu.ru/ru/viewschedule/" + str(number) + '//'
    page = request.urlopen(url=group)
    soup = BeautifulSoup(page, 'lxml')
    table = soup.find('table')
    table_tr = table.find_all('tr')
    for row in table_tr:
        body = row.contents
        for string in body:
            if string == '\n': pass
            else:
                for i in string.contents:
                    try:
                        if i.contents:
                            if type(i.contents) is list:
                                data.append(i.contents[0])
                            else: data.append(i.contents)
                    except:
                        data.append(i)
    return data[3:], number


def parse_data(records, number):
    temp_work = ''
    virus_words = ['Время', 'Аудитория', 'Предмет', 'Преподаватель']
    shedule = {'Понедельник':[], 'Вторник':[], 'Среда':[], 'Четверг':[], 'Пятница':[], 'Суббота':[]}
    for record in records:
        if record in virus_words:
            continue
        elif record in shedule.keys():
            if temp_work: shedule[day_week].append(temp_work)
            day_week = record
            temp_work = ''
        elif record[0].isdecimal():
            if temp_work: shedule[day_week].append(temp_work)
            temp_work = number + ', ' + record
        elif record == records[-1]:
            temp_work += ', ' + record
            if temp_work: shedule[day_week].append(temp_work)
        else:
            temp_work += ', ' + record
    return shedule


def poisk_data(poisk, shedule_file_name='all_shedule', numbers_group_file_name='numbers_groups'):
    result = {'Понедельник':[], 'Вторник':[], 'Среда':[], 'Четверг':[], 'Пятница':[], 'Суббота':[]}
    numbers = load_obj(numbers_group_file_name)
    shedule = load_obj(shedule_file_name)
    for group in numbers:
        for day in result.keys():
            for rows in shedule[group][day]:
                if poisk in rows:
                    result[day].append(rows)
    return clear(result)


def save_obj(obj, name ):
    with open('./' + name + '.pkl', 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)


def load_obj(name ):
    with open('./' + name + '.pkl', 'rb') as f:
        return pickle.load(f)


def dump_site(name_file='all_shedule', file_numbers='numbers_groups'):
    shedule = {}
    parse_numbers_groups()
    for group in load_obj(file_numbers):
        data, number = parse_group(group)
        group_shedule = parse_data(data, number)
        shedule[group] = group_shedule
    save_obj(shedule, name_file)


def dump_numbers_groups(file_name):
    numbers_groups = parse_numbers_groups()
    save_obj(numbers_groups, file_name)

def output(keyword):
    days = ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота']
    message = ''
    poisk = keyword
    if poisk == 'dump':
        dump_site()
        message += '[+] Сайт успешно скопирован!'
        return message
    else:
        shedule = poisk_data(poisk)
        message += '\n'
        for i in days:
            message += '\n'
            if i == 'Понедельник':
                message += '*' * 7 + i + '*' * 5
            elif i == 'Среда':
                message += '*' * 7 + i + '*' * 8
            else:
                message += '*' * 7 + i + '*' * 7
            for j in shedule[i]:
                message += '\n[+] ' + re.sub("^\s+|\n|\r|\t|\s+$", '', j)
        message += '\n\n[=] Поиск успешно произведен!'
        return message

if __name__ == '__main__':
        print('Добро пожаловать в программу для сортировки расписания')
        print('Если программа запущена первый раз введите в строку поиска dump')
        print('Чтобы получить актуальное расписание с сайта, это займет ~2 минуты')
        print('Поиск можно совершать по любому вашему желанию (только одному)')
        print('Для поиска можно использовать только РУССКИЕ БУКВЫ и ЦИФРЫ')
        print(output(input('Введите строку для поиска: ')))
        input('Нажмите Enter для завершения программы')
